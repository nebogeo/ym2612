## YM2612 experiments

experimental code for the YM2612 synth chip, contains:

* atmega328 driver code, tested and works (schematics and other details to follow)
* arduino driver version for nano, didn't work for me but I know why (now) and it might be fixed...
* software emulation from MAME/Genesis Plus GX

Copyright (C) 2001, 2002, 2003 Jarek Burczynski (bujar at mame dot net)
Copyright (C) 1998 Tatsuyuki Satoh , MultiArcadeMachineEmulator development

software emulation is a jack client with a midi interface


