#include "jellyfish/types.h"
#include <portmidi.h>

#pragma once

namespace spiralcore {

class midi {
 public:
  static void init();

  midi(bool output, u32 device);

  void send(u8* data, u32 len);
  void receive(u8* data, u32 len);

  static u8 note_on[3];
  static u8 all_stop[3];

 private:
  PmStream *midi_out;
  PmStream *midi_in;

};

}
