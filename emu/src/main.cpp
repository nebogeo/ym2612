#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include "ym2612.h"
#include "midi.h"
#include "JackClient.h"

using namespace spiralcore;
using namespace std;

float *m_jack_buffer;
pthread_mutex_t* m_mutex;
int *int_buffer;

void jack_callback(void *context, unsigned int size) {
  //flex **f=static_cast<flex**>(context);

  for (u32 i=0; i<size; i++) {
    m_jack_buffer[i]=0;
  }

  if (!pthread_mutex_trylock(m_mutex)) {
    YM2612Update(int_buffer,size);
    pthread_mutex_unlock(m_mutex);
  } 

  for (int i=0; i<size; ++i) {
    m_jack_buffer[i]=0.5f*int_buffer[i*2]/(float)SHRT_MAX;
  }

}


void connect_jack() {
  u32 BufferLength=4096;
  m_jack_buffer = new float[BufferLength];
  int_buffer = new int[BufferLength*2];
  
  int temp=0;
  JackClient *Jack = JackClient::Get();
  Jack->SetCallback(jack_callback,&temp);
  Jack->Attach("ym2612");
  if (Jack->IsAttached()) {	
    int id=Jack->AddOutputPort();
    Jack->SetOutputBuf(id,m_jack_buffer);
    Jack->ConnectOutput(id, "system:playback_1");
    //Jack->ConnectInput(id, "hexter DX7 emulation (v1.0.3):hexter DX7 emulation (v1.0.3) out_1");
    cerr<<"Attached to jack"<<endl;
  } else {
    cerr<<"Could not attach to jack"<<endl;
  }
}

unsigned char test_patch[] = {
  0x00,0x00,
  0x00,
  0x00,
  0x71,0x0d,0x33,0x01,
  0x23,0x2d,0x26,0x00,
  0x5f,0x99,0x5f,0x94,
  0x05,0x05,0x05,0x07,
  0x02,0x02,0x02,0x02,
  0x11,0x11,0x11,0xa6,
  0x00,0x00,0x00,0x00,
  0x32,0xc0,
  0x00,
  0x22,0x69,
  0xf0,
}; 

unsigned char test_addr[] = {
  0x22,0x27,
  0x28,
  0x2b,
  0x30,0x34,0x38,0x3c,
  0x40,0x44,0x48,0x4c,
  0x50,0x54,0x58,0x5c,
  0x60,0x64,0x68,0x6c,
  0x70,0x74,0x78,0x7c,
  0x80,0x84,0x88,0x8c,
  0x90,0x94,0x98,0x9c,
  0xb0,0xb4,
  0x28,
  0xa4,0xa0,
  0x28
};

void play(unsigned char v) {
  YM2612Write(0,0x28);
  YM2612Write(1,0x00|v);
  YM2612Write(0,0x28);
  YM2612Write(1,0xF0|v);
}

int main(int argc, char *argv[]) {
  midi::init();
  srand(time(NULL));
  midi input_midi(false,1);

  float midi_note[127];
  float a = 440.0f; // a is 440 hz...
  for (int x = 0; x < 127; ++x) {
    midi_note[x] = (a / 32.0f) * powf(2.0f,((x - 9.0f) / 12.0f));
  }

  m_mutex = new pthread_mutex_t;
  pthread_mutex_init(m_mutex,NULL);

  YM2612Init();
  YM2612Config(9);
  YM2612ResetChip();

  connect_jack();

  u32 data_size=(0xb6-0x21)*2*2;
  cerr<<"data size="<<data_size<<endl;
  u8 midi_data[data_size+100];

  unsigned int timer=0;

  for (int i=0; i<38; ++i) {    
    YM2612Write(0,test_addr[i]);
    YM2612Write(1,test_patch[i]); 
  }

  while(1) {
    timer++;

    midi_data[0]=0;
    input_midi.receive(midi_data,data_size+100);
    if (midi_data[0]!=0) {
      //cerr<<"midi: "<<(int)midi_data[0]<<" "<<
      //	(int)midi_data[1]<<" "<<
      //	(int)midi_data[2]<<endl;
      u8 midi_type = midi_data[0]>>4;
      u8 midi_chan = midi_data[0]&0x0f;

      //cerr<<(int)midi_data[0]<<" "<<(int)midi_type<<" "<<(int)midi_chan<<endl;
      
      switch(midi_type) {
      case 0x9: { // play note      
	cerr<<"play"<<endl;
	pthread_mutex_lock(m_mutex);
	//YM2612ResetChip();
	YM2612Write(0,0x28);
	YM2612Write(1,0xF0);
	/*	YM2612Write(1,0xF1);
	YM2612Write(1,0xF2);
	YM2612Write(1,0xF3);
	YM2612Write(1,0xF4);
	YM2612Write(1,0xF5);*/
	pthread_mutex_unlock(m_mutex);
      } break;
      case 0x8: { // stop note
	cerr<<"stop"<<endl;
	pthread_mutex_lock(m_mutex);
	YM2612Write(0,0x28);
	YM2612Write(1,0x00);
	/*	YM2612Write(1,0x01);
	YM2612Write(1,0x02);
	YM2612Write(1,0x03);
	YM2612Write(1,0x04);
	YM2612Write(1,0x05);*/
	pthread_mutex_unlock(m_mutex);
      } break;
      case 0xf: {
	cerr<<"sysex"<<endl;
	pthread_mutex_lock(m_mutex);
	u8 addr = midi_data[3]|(midi_data[4]<<4);
	u8 data = midi_data[5]|(midi_data[6]<<4);
	cerr<<std::hex<<(unsigned int)addr<<"<-"<<(unsigned int)data<<endl;
	YM2612Write(0,addr);
	YM2612Write(1,data);

	// u32 pos=2;
	// // this will set all voices in one go
	// // part 1
	// for (u32 addr=0x21; addr<0xb6; addr++) {
	//   YM2612Write(0,addr);
	//   u8 dat = midi_data[pos++]|(midi_data[pos++]<<4);
	//   YM2612Write(1,dat);
	// }

	// // part 2
	// for (u32 addr=0x21; addr<0xb6; addr++) {
	//   YM2612Write(2,addr);
	//   u8 dat = midi_data[pos++]|(midi_data[pos++]<<4);
	//   YM2612Write(1,dat);
	// }

	pthread_mutex_unlock(m_mutex);
      } break;
      }
    }
    usleep(1000);
  }
  
  return 0;
}
