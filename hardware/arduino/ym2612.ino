// concept driver code for the ym2612 for running on arduino nano

// not yet working, is it:
// * bad register writes?
// * bad clock? (check via xtal) 
// * bad chip?

int led = 13;

// physical ctrl pins
const int YM_CTRL_IC = B00000001;
const int YM_CTRL_CS = B00000010;
const int YM_CTRL_WR = B00000100;
const int YM_CTRL_RD = B00001000;
const int YM_CTRL_A0 = B00010000;
const int YM_CTRL_A1 = B00100000;
const int YM_CTRL_INIT = YM_CTRL_IC | YM_CTRL_CS | YM_CTRL_WR | YM_CTRL_RD;

// internal registers
const int YM_REG_LFO = 0x22;
const int YM_REG_TIMERA_MSB = 0x24;
const int YM_REG_TIMERA_LSB = 0x25;
const int YM_REG_TIMERB = 0x26;
const int YM_REG_TIMERCH36MODE = 0x27;
const int YM_REG_KEY = 0x28;
const int YM_REG_DAC = 0x2A;
const int YM_REG_DAC_ENABLE = 0x2B;

// operator registers follow
const int YM_REG_DETUNE_MULT = 0x30;
const int YM_REG_LEVEL = 0x40;
const int YM_REG_RATE_ATTACK = 0x50;
const int YM_REG_AM_DECAY = 0x60;
const int YM_REG_SECONDARY_DECAY = 0x70;

const int YM_VAL_LFO_ENABLE = B00001000;
const int YM_VAL_KEY_ON = 0xf0;
const int YM_VAL_KEY_OFF = 0x00;

// piano sound
unsigned char test_patch[] = {
  0x00,0x00,
  0x00,
  0x00,
  0x71,0x0d,0x33,0x01,
  0x23,0x2d,0x26,0x00,
  0x5f,0x99,0x5f,0x94,
  0x05,0x05,0x05,0x07,
  0x02,0x02,0x02,0x02,
  0x11,0x11,0x11,0xa6,
  0x00,0x00,0x00,0x00,
  0x32,0xc0,
  0x00,
  0x22,0x69,
  0xf0,
}; 

unsigned char test_addr[] = {
  0x22,0x27,
  0x28,
  0x2b,
  0x30,0x34,0x38,0x3c,
  0x40,0x44,0x48,0x4c,
  0x50,0x54,0x58,0x5c,
  0x60,0x64,0x68,0x6c,
  0x70,0x74,0x78,0x7c,
  0x80,0x84,0x88,0x8c,
  0x90,0x94,0x98,0x9c,
  0xb0,0xb4,
  0x28,
  0xa4,0xa0,
  0x28
};

void send_8bits(unsigned char v) {
  DDRD = B00111100;
  DDRB = B00011111;
  PORTD = (v&0x0f)<<2;
  PORTB = (v&0x70)>>4 | (v&0x80)>>3;
}

unsigned char read_8bits() {
  DDRD = B00000000;
  DDRB = B00001000;
  unsigned char v = (PIND&0x3c)>>2;
  v|=(PINB&0x07)<<4 | (PINB&0x10)<<3;
  return v;
}

bool ym_busy() {
  unsigned char v = ym_read_part1();
  return 0x80&v;
  //return false;
}

// raw read/write - should leave registers in same state
// probably too many delays...
void ym_write(unsigned char val) {
  delayMicroseconds(2); 
  PORTC &= ~YM_CTRL_CS; // chip select = 0 is ON  
  delayMicroseconds(2); 
  send_8bits(val); 
  delayMicroseconds(2); 
  PORTC &= ~YM_CTRL_WR; // write = 0 is ON - RISING EDGE LATCHES DATA INTO CHIP
  delayMicroseconds(2);  
  PORTC |= YM_CTRL_WR; // write = 1 is OFF
  delayMicroseconds(2); 
  PORTC |= YM_CTRL_CS; // chip select = 1 is OFF
  delayMicroseconds(2);  
}

unsigned char ym_read() {
  delayMicroseconds(2);  
  PORTC &= ~YM_CTRL_CS; // chip select = 0 is ON
  delayMicroseconds(2); 
  PORTC &= ~YM_CTRL_RD; // read = 0 is ON - RISING EDGE OUTPUTS DATA FROM CHIP
  delayMicroseconds(2);  
  unsigned char ret = read_8bits();    
  delayMicroseconds(2); 
  PORTC |= YM_CTRL_CS; // chip select = 1 is OFF
  delayMicroseconds(2); 
  PORTC &= ~YM_CTRL_RD; // read = 1 is OFF
  return ret;
}

void ym_write_part1(unsigned char addr, unsigned char value) {
  Serial.print("writing ");
  Serial.print(value);
  Serial.print(" to ");
  Serial.println(addr);
  
//  while(ym_busy()) { Serial.println("busy..."); delay(100); }

  PORTC = YM_CTRL_INIT;
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to write address first
  ym_write(addr);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A0; // A0=1 - sets registers to write data second
  ym_write(value);

  PORTC = YM_CTRL_INIT;
}

void ym_write_part2(unsigned char addr, unsigned char value) {
  Serial.print("writing ");
  Serial.print(value);
  Serial.print(" to ");
  Serial.println(addr);
  
  PORTC = YM_CTRL_INIT;
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to write address first
  PORTC |= YM_CTRL_A1; 
  ym_write(addr);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A0; // A0=1 - sets registers to write data second
  PORTC |= YM_CTRL_A1; 
  ym_write(value);

  PORTC = YM_CTRL_INIT;
}

unsigned char ym_read_part1() {
  // write address
  // address is immaterial but still needs to be sent??
  PORTC = YM_CTRL_INIT;
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to read address first
  ym_write(0x0f);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A0; 
  return ym_read();
}

int clock_pin=11; // OC2A pin on the nano

// If you change the prescale value, it affects CS22, CS21, and CS20
// For a given prescale value, the eight-bit number that you
// load into OCR2A determines the frequency according to the
// following formulas:
//
// With no prescaling, an ocr2val of 3 causes the output pin to
// toggle the value every four CPU clock cycles. That is, the
// period is equal to eight slock cycles.
//
// With F_CPU = 16 MHz, the result is 2 MHz.
//
// Note that the prescale value is just for printing; changing it here
// does not change the clock division ratio for the timer!  To change
// the timer prescale division, use different bits for CS22:0 below
const int prescale  = 1;
//const int ocr2aval  = 3;
const int ocr2aval  = 3;

// The following are scaled for convenient printing
//
// Period in microseconds
const float period    = 5.0 * prescale * (ocr2aval+1) / (F_CPU/1.0e6);

// Frequency in Hz
const float freq      = 1.0e6 / period;

void start_clock() { 
   pinMode(clock_pin, OUTPUT);
   Serial.begin(9600);

    // Set Timer 2 CTC mode with no prescaling.  OC2A toggles on compare match
    //
    // WGM22:0 = 010: CTC Mode, toggle OC
    // WGM2 bits 1 and 0 are in TCCR2A,
    // WGM2 bit 2 is in TCCR2B
    // COM2A0 sets OC2A (arduino pin 11 on Uno or Duemilanove) to toggle on compare match
    //
    TCCR2A = ((1 << WGM21) | (1 << COM2A0));

    // Set Timer 2  No prescaling  (i.e. prescale division = 1)
    //
    // CS22:0 = 001: Use CPU clock with no prescaling
    // CS2 bits 2:0 are all in TCCR2B
    TCCR2B = (1 << CS20);

    // Make sure Compare-match register A interrupt for timer2 is disabled
    TIMSK2 = 0;
    // This value determines the output frequency
    OCR2A = ocr2aval;

    Serial.print("Period    = ");
    Serial.print(period);
    Serial.println(" microseconds");
    Serial.print("Frequency = ");
    Serial.print(freq);
    Serial.println(" Hz");
}

void scramble() {
  for (unsigned int i=0x22; i<0xb5; ++i) {    
    ym_write_part1(i,random()%255);
  }
}

// the setup routine runs once when you press reset:
void setup() {      
  
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     

  // port c is the control register  
  DDRC = 0xff;
  PORTC = YM_CTRL_INIT;

  start_clock();

  // reset
  PORTC &= ~YM_CTRL_IC;
  delay(100);
  PORTC |= YM_CTRL_IC;
  delay(300);
  
//  for (unsigned int i=0; i<38; ++i) {    
//    ym_write_part1(test_addr[i],test_patch[i]);
//  }
  
  // should start clock b, resulting in an overflow bit set
  ym_write_part1(0x27,0x0a);
  
}

// the loop routine runs over and over again forever:
void loop() {
  //scramble();
  
  Serial.print((int)ym_read_part1());
  Serial.println("<- loop status");
   
//  ym_write_part1(0x28,0x00);
//  ym_write_part1(0x28,0xF0);
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second

  //  ym_write_part1(0x26,0x00);

}
