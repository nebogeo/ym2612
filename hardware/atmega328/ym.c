#define F_CPU F_OSC

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

// physical ctrl pins
#define YM_CTRL_IC 0b00100000
#define YM_CTRL_CS 0b00010000
#define YM_CTRL_WR 0b00001000
#define YM_CTRL_RD 0b00000100
#define YM_CTRL_A0 0b00000010
#define YM_CTRL_A1 0b00000001
#define YM_CTRL_INIT (YM_CTRL_IC | YM_CTRL_CS | YM_CTRL_WR | YM_CTRL_RD)

#define YM_MCLOCK (1) // PB1 = OC1A (= pin D9 for Arduino UNO)
#define YM_MCLOCK_DDR DDRB
#define STATUS_PORT PORTB
#define STATUS (2)

// internal registers
#define YM_REG_LFO 0x22
#define YM_REG_TIMERA_MSB 0x24
#define YM_REG_TIMERA_LSB 0x25
#define YM_REG_TIMERB 0x26
#define YM_REG_TIMERCH36MODE 0x27
#define YM_REG_KEY 0x28
#define YM_REG_DAC 0x2A
#define YM_REG_DAC_ENABLE 0x2B

// operator registers follow
#define YM_REG_DETUNE_MULT 0x30
#define YM_REG_LEVEL 0x40
#define YM_REG_RATE_ATTACK 0x50
#define YM_REG_AM_DECAY 0x60
#define YM_REG_SECONDARY_DECAY 0x70

#define YM_VAL_LFO_ENABLE 0b00001000
#define YM_VAL_KEY_ON 0xf0
#define YM_VAL_KEY_OFF 0x00

// piano sound
unsigned char test_patch[] = {
  0x00,0x00,
  0x00,
  0x00,
  0x71,0x0d,0x33,0x01,
  0x23,0x2d,0x26,0x00,
  0x5f,0x99,0x5f,0x94,
  0x05,0x05,0x05,0x07,
  0x02,0x02,0x02,0x02,
  0x11,0x11,0x11,0xa6,
  0x00,0x00,0x00,0x00,
  0x32,0xc0,
  0x00,
  0x22,0x69,
  0xf0,
}; 

unsigned char test_addr[] = {
  0x22,0x27,
  0x28,
  0x2b,
  0x30,0x34,0x38,0x3c,
  0x40,0x44,0x48,0x4c,
  0x50,0x54,0x58,0x5c,
  0x60,0x64,0x68,0x6c,
  0x70,0x74,0x78,0x7c,
  0x80,0x84,0x88,0x8c,
  0x90,0x94,0x98,0x9c,
  0xb0,0xb4,
  0x28,
  0xa4,0xa0,
  0x28
};

void send_8bits(unsigned char v) {
  PORTD = v;
}

unsigned char read_8bits(void) {
  DDRD = 0x00; // all input
  _delay_us(2); 
  unsigned char t = PIND;
  DDRD = 0xff; // all output
  return t;
}

// raw read/write - should leave registers in same state
// probably too many delays...
void ym_write(unsigned char val) {
  PORTC &= ~YM_CTRL_CS; // chip select = 0 is ON  
  send_8bits(val); 
  _delay_us(1);  
  PORTC &= ~YM_CTRL_WR; // write = 0 is ON - RISING EDGE LATCHES DATA INTO CHIP
  _delay_us(5); 
  PORTC |= YM_CTRL_WR; // write = 1 is OFF
  PORTC |= YM_CTRL_CS; // chip select = 1 is OFF
}

unsigned char ym_read(void) {
  PORTC &= ~YM_CTRL_CS; // chip select = 0 is ON
  _delay_us(1);  
  PORTC &= ~YM_CTRL_RD; // read = 0 is ON - RISING EDGE OUTPUTS DATA FROM CHIP
  _delay_us(5);  
  unsigned char ret = read_8bits();    
  PORTC |= YM_CTRL_RD; // read = 1 is OFF
  PORTC |= YM_CTRL_CS; // chip select = 1 is OFF
  return ret;
}

void ym_write_part1(unsigned char addr, unsigned char value) {  
  PORTC = YM_CTRL_INIT;
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to write address first
  ym_write(addr);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A0; // A0=1 - sets registers to write data second
  ym_write(value);

  PORTC = YM_CTRL_INIT;
}

void ym_write_part2(unsigned char addr, unsigned char value) {  
  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A1; // set A1 high to select the second register bank
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to write address first
  ym_write(addr);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A1; // set A1 high to select the second register bank
  PORTC |= YM_CTRL_A0; // A0=1 - sets registers to write data second
  ym_write(value);

  PORTC = YM_CTRL_INIT;
}

unsigned char ym_read_part1(void) {
  // write address
  // address is immaterial but still needs to be sent??
  PORTC = YM_CTRL_INIT;
  PORTC &= ~YM_CTRL_A0; // A0=0 - sets registers to read address first
  ym_write(0x0f);

  PORTC = YM_CTRL_INIT;
  PORTC |= YM_CTRL_A0; 
  return ym_read();
}

unsigned char ym_busy(void) {
  return 0x80&ym_read_part1();
}

void scramble(void) {
  for (uint8_t i=0; i<2; ++i) {    
    ym_write_part1(0x30+random()%(0x20),random()%255);
  }
}

// the setup routine runs once when you press reset:
void setup(void) {      
  //disable interrupts
  cli();
  MCUSR = 0;
  wdt_disable();

  YM_MCLOCK_DDR |= _BV(YM_MCLOCK) | _BV(STATUS);
  
  // port c is the control register  
  DDRC = 0xff;
  PORTC = YM_CTRL_INIT;
  DDRD = 0xff; // data
  // port b for debugging stuff
  //  DDRB = 0xff;

  PORTB |= 0x4;

  /* F_CPU / 2 clock generation */
  TCCR1A = _BV(COM1A0);            /* Toggle OCA1 on compare match */
  TCCR1B = _BV(WGM12) | _BV(CS10); /* CTC mode with prescaler /1 */
  TCCR1C = 0;                      /* Flag reset */
  TCNT1 = 0;                       /* Counter reset */
  OCR1A = 0;                       /* Divide base clock by two */

  // reset
  PORTC &= ~YM_CTRL_IC;
  _delay_ms(10);
  PORTC |= YM_CTRL_IC;
  _delay_ms(100);

  for (unsigned int i=0; i<38; ++i) {    
    ym_write_part1(test_addr[i],test_patch[i]);
  }

  PORTB &= ~0x4;

  // should start clock b, resulting in an overflow bit set
  //ym_write_part1(0x27,0x0a);
  
}

int main(void) {
  setup();
  
  for (;;) {
    wdt_reset();
    scramble();
    //    unsigned char t=ym_read_part1();

    ym_write_part1(0x28,0xF0);    
    ym_write_part1(0x28,0xF1);    
    ym_write_part1(0x28,0xF2);    
    _delay_ms(100);

    //PORTB &= ~0x4;
    ym_write_part1(0x28,0x00);
    ym_write_part1(0x28,0x01);
    ym_write_part1(0x28,0x02);
    _delay_ms(100);
  }
}
