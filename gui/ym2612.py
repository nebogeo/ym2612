import time
import rtmidi
import random
import sys
import platform
import copy
import math

if int(platform.python_version_tuple()[0])>2:
	from tkinter import *
	from tkinter.filedialog import *
	from tkinter.messagebox import *
else:
	from Tkinter import *
	from tkFileDialog import *
	from tkMessageBox import *

version = "0.0.1"

def sq(x): return x*x

# global params
# 0x22 3   LFO enable
# 0x22 0-2 LFO freq
# 0x27 7   Ch3 mode
# 0x28 0-3 Key on/off channel
# 0x28 4-7 Key on/off operator

# channel/operator params [*3*4]
# 0x30+ 0-3 freq multiplier
# 0x30+ 4-6 freq detune
# 0x40+ 0-6 volume
# 0x50+ 0-4 attack rate
# 0x50+ 6-7 rate scaling (env speed with pitch?)
# 0x60+ 0-4 decay rate
# 0x60+ 7   LFO AM
# 0x70+ 0-4 secondary decay rate
# 0x80+ 0-4 release rate
# 0x80+ 5-7 secondary amplitude

# channel params [*3]
# 0xa0+ 0-7 freq lsb 
# 0xa4+ 0-2 freq msb
# 0xa8+ 0-7 ch3 supplimentary freq num?
# 0xac+ 0-2 ch3 supplimentary freq num?
# 0xac+ 3-5 ch3 supplimentary block?
# 0xb0+ 0-2 algorithm
# 0xb0+ 3-5 feedback
# 0xb4+ 0-1 lfo freq mod sensitivity
# 0xb4+ 3-5 lfo amplitude mod sensitivity
# 0xb4+ 6   right output on/off 
# 0xb4+ 7   left output on/off 

class bitfield:
    bf_id = 0
    def __init__(self,addr,start,end):
        self.addr=addr
        self.start=start
        self.end=end
        self.size=(end-start)+1
        self.current=0
        self.bf_id = bitfield.bf_id
        bitfield.bf_id+=1

class fmstate:
    def __init__(self):
        self.glob_params=[]
        self.glob_params.append(["lfo_enable",bitfield(0x22,3,3)])
        self.glob_params.append(["lfo_freq",bitfield(0x22,0,2)])
        self.glob_params.append(["ch3_mode",bitfield(0x27,7,7)])
        self.glob_params.append(["key_channel",bitfield(0x28,0,3)])
        self.glob_params.append(["key_operator",bitfield(0x28,4,7)])

        self.op_params=[[[],[],[],[]],
                        [[],[],[],[]],
                        [[],[],[],[]]]

        for op in range(0,4):
            for ch in range(0,3):
                self.op_params[ch][op].append(["freq_mul",bitfield(0x30+op*4+ch,0,3)])
                self.op_params[ch][op].append(["freq_detune",bitfield(0x30+op*4+ch,4,6)])
                self.op_params[ch][op].append(["volume",bitfield(0x40+op*4+ch,0,6)])
                self.op_params[ch][op].append(["attack_rate",bitfield(0x50+op*4+ch,0,4)])
                self.op_params[ch][op].append(["rate_scale",bitfield(0x50+op*4+ch,6,7)])
                self.op_params[ch][op].append(["decay_rate",bitfield(0x60+op*4+ch,0,4)])
                self.op_params[ch][op].append(["lfo_am",bitfield(0x60+op*4+ch,7,7)])
                self.op_params[ch][op].append(["decay_rate2",bitfield(0x70+op*4+ch,0,4)])
                self.op_params[ch][op].append(["release_rate",bitfield(0x80+op*4+ch,0,4)])
                self.op_params[ch][op].append(["amplitude2",bitfield(0x80+op*4+ch,5,7)])

        self.ch_params=[[],[],[],[]]

        for ch in range(0,3):
            self.ch_params[ch].append(["freq_lsb",bitfield(0xa0+ch,0,7)])
            self.ch_params[ch].append(["freq_msb",bitfield(0xa4+ch,0,2)])
            self.ch_params[ch].append(["ch3_freq1",bitfield(0xa4+ch,0,7)])
            self.ch_params[ch].append(["ch3_freq2",bitfield(0xac+ch,0,2)])
            self.ch_params[ch].append(["ch3_block",bitfield(0xac+ch,3,5)])
            self.ch_params[ch].append(["algorithm",bitfield(0xb0+ch,0,2)])
            self.ch_params[ch].append(["feedback",bitfield(0xb0+ch,3,5)])
            self.ch_params[ch].append(["lfo_freq_sens",bitfield(0xb4+ch,0,1)])
            self.ch_params[ch].append(["lfo_amp_sens",bitfield(0xb4+ch,3,5)])
            self.ch_params[ch].append(["left_out",bitfield(0xb4+ch,6,6)])
            self.ch_params[ch].append(["right_out",bitfield(0xb4+ch,7,7)])
        
    def get_sysex(self):
        sysex=[]
        return sysex

    def from_sysex(self,sysex):
        pos = 0

############################################################

class env_canvas:
    def __init__(self,master,y,x):
        self.canvas = Canvas(master, width=200, height=25)
        self.canvas.grid(row=y, column=x)

    def set(self,env):
        self.canvas.delete(ALL) # remove all items
        pts=[]
        for e in env:
            pts.append(e)
        pts=sorted(pts, key=lambda p: p[0])
        pts[0][0]=0
        lx=0
        ly=0
        for e in pts:
            self.canvas.create_line(lx, ly, e[0]*200, 25-e[1]*25)
            lx=e[0]*200
            ly=25-e[1]*25

class win:
    def __init__(self):
        self.midi_ready=True
        self.scales={}

        self.memory = []
        for i in range(0,255): self.memory.append(0)

        # create window
        self.root = Tk()
        self.root.title("ym2612 gui "+version)
        top = Frame(self.root)
        top.pack(side=RIGHT)

        self.fmstate = fmstate()

        f=Frame(top).pack(side=LEFT)
        Label(f, text="global").pack() 
        for t,s in self.fmstate.glob_params:
            self.build_param(f,t,s)

        f=Frame(top, bg="red")
        Label(f, text="operator 1").pack() 
        for t,s in self.fmstate.op_params[0][0]:
            self.build_param(f,t,s)
        f.pack(side=LEFT)

        f=Frame(top, bg="green")
        Label(f, text="operator 2").pack() 
        for t,s in self.fmstate.op_params[0][1]:
            self.build_param(f,t,s)
        f.pack(side=LEFT)

        f=Frame(top, bg="blue")
        Label(f, text="operator 3").pack() 
        for t,s in self.fmstate.op_params[0][2]:
            self.build_param(f,t,s)
        f.pack(side=LEFT)

        f=Frame(top, bg="yellow")
        Label(f, text="operator 4").pack() 
        for t,s in self.fmstate.op_params[0][3]:
            self.build_param(f,t,s)
        f.pack(side=LEFT)

        f=Frame(top)
        Label(f, text="channel").pack() 
        for t,s in self.fmstate.ch_params[0]:
            self.build_param(f,t,s)
        f.pack(side=LEFT)

        Button(f, text="X", command=self.play).pack()
#        Button(f, text="load", command=self.load).grid(row=21, column=3)
#        Button(f, text="save", command=self.save).grid(row=21, column=4)

        f=Frame(top)
        f.pack()
            
        self.midiout = rtmidi.MidiOut()
        available_ports = self.midiout.get_ports()
        print(available_ports)
        if available_ports:
            self.midiout.open_port(0)
        else:
            self.midiout.open_virtual_port("My virtual output")

    def save(self):
        filename = asksaveasfilename(title = "save patch")
        if filename!="":
            with open(filename, 'wb') as output:
                    output.write(bytearray(self.patch.get_sysex()))

    def load(self):
        filename = askopenfilename(title = "load patch")
        if filename!="":
            with open(filename, 'rb') as infile:
                #for p in range(0,len(self.patches)):
                self.patch.from_sysex(bytearray(infile.read(90)))
        self.read_patch()

    def note_on(self,channel):
        self.midiout.send_message([0x80|channel,60,0])
        self.midiout.send_message([0x90|channel,60,112])

    def patch_callback(self,v):
        self.current_patch=int(self.patchv.get())
        self.patch = self.patches[self.current_patch]
        self.read_patch()

    def play(self):
        #self.write_patch()
        self.note_on(0)

    def update_param(self,t,bf,v):
        mask = 0xff>>(8-bf.size)
        mask = mask<<bf.start 
        self.memory[bf.addr] = (~mask&self.memory[bf.addr]) | (mask & (int(v)<<bf.start))
        print bin(self.memory[bf.addr])
        #self.midiout.send_message([0xf0,00,bf.addr,self.memory[bf.addr],0xf7])
        self.midiout.send_message([0xf0,00,
                                   bf.addr&0x0f,(bf.addr>>4)&0x0f,
                                   self.memory[bf.addr]&0x0f,(self.memory[bf.addr]>>4)&0x0f,
                                   0xf7])

    def find_bitfields(self,addr):
        ret = []
        for name,bf in self.fmstate.glob_params:
            if bf.addr==addr: ret.append(bf)
        for l in self.fmstate.op_params[0]:
            for name,bf in l: 
                if bf.addr==addr: ret.append(bf)
        for name,bf in self.fmstate.ch_params:
            if bf.addr==addr: ret.append(bf)
        return ret

    def read_param(self,addr,v):
        # search for bit fields at this address 
        for bf in self.find_bitfields(addr):
            mask = 0xff>>(8-bf.size)
            mask = mask<<bf.start 
            val = (v&mask)>>bf.start
            self.scales[bf.bf_id].set(val)

    def build_param(self,parent,t,bf):
        f=Frame(parent)
        f.pack(fill=X)
        top = (2**bf.size)-1
        w = Scale(f, from_=0, to=top, orient=HORIZONTAL, command=lambda v: self.update_param(t,bf,v))
        self.scales[bf.bf_id]=w
        w.pack()
        Label(f, text=t).pack()
    
    def read(self,memory):
        self.memory=memory
        for addr in range(0,255):
            self.read_param(addr,self.memory[addr])

#############################################################

w = win()

try:
    w.root.mainloop()
except Exception,e:
    print(e)



